#include "test.h"
#include "mem.h"
#include "mem_internals.h"
#include <sys/mman.h>

#define HEAP_SIZE 8192
#define MALLOC_SIZE 1024

bool first() {
    heap_init(HEAP_SIZE);
    debug_heap(stdout, HEAP_START);
    void* block = _malloc(MALLOC_SIZE);
    debug_heap(stdout, HEAP_START);
    if(!block) {
        return false;
    }
    _free(block);
    debug_heap(stdout, HEAP_START);
    return true;

}

bool second() {
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    if (heap == NULL) {
        return false;
    }

    void *malloc_result_0 = _malloc(MALLOC_SIZE);
    debug_heap(stdout, heap);
    void *malloc_result_1 = _malloc(MALLOC_SIZE);
    debug_heap(stdout, heap);
    void *malloc_result_2 = _malloc(MALLOC_SIZE);
    debug_heap(stdout, heap);

    if (malloc_result_0 == NULL || malloc_result_1 == NULL || malloc_result_2 == NULL) {
        return false;
    }

    _free(malloc_result_1);
    debug_heap(stdout, heap);

    if (malloc_result_0 == NULL || malloc_result_2 == NULL) {
        return false;
    }

    _free(malloc_result_0);
    _free(malloc_result_2);
    munmap(heap, size_from_capacity((block_capacity) {REGION_MIN_SIZE}).bytes);
    return true;
}

bool third() {
    heap_init(HEAP_SIZE);
    debug_heap(stderr, HEAP_START);
    _malloc(1);
    void *addr1 = _malloc(1953);
    void *addr2 = _malloc(786);
    _malloc(1);
    debug_heap(stderr, HEAP_START);
    _free(addr1);
    debug_heap(stderr, HEAP_START);
    _free(addr2);
    debug_heap(stderr, HEAP_START);
    munmap(HEAP_START, size_from_capacity((block_capacity) { HEAP_SIZE }).bytes);
    return true;

}

bool fourth() {
    heap_init(HEAP_SIZE);
    void* block_1 = _malloc(5000);
    debug_heap(stdout, HEAP_START);
    void* block_2 = _malloc(9000);
    if (!(block_1 || block_2)) {
        return 0;
    }
    debug_heap(stdout, HEAP_START);
    _free(block_1);
    _free(block_2);
    munmap(HEAP_START, size_from_capacity((block_capacity) { HEAP_SIZE }).bytes);
    return true;
}

bool fifth() {
    heap_init(HEAP_SIZE);
    debug_heap(stderr, HEAP_START);

    void *addr1 = _malloc(HEAP_SIZE);
    debug_heap(stderr, HEAP_START);
    _malloc(1);
    void *addr2 = _malloc(HEAP_SIZE);
    debug_heap(stderr, HEAP_START);

    if (addr2 == addr1 + HEAP_SIZE + offsetof(struct block_header, contents)) {
        return false;
    }

    munmap(HEAP_START, size_from_capacity((block_capacity) { HEAP_SIZE }).bytes);
    munmap(addr1 + HEAP_SIZE, size_from_capacity((block_capacity) { 1 }).bytes);
    munmap(addr2 - offsetof(struct block_header, contents), size_from_capacity((block_capacity) { HEAP_SIZE }).bytes);
    return true;
}

int run_tests() {
    bool (*test_func[5])() = { fifth, second, third, fourth, fifth };
    int counter = 0;
    for (int i = 0; i < 5; ++i) {
        if (test_func[i]()) {
            ++counter;
        }
    }
    return counter;
}